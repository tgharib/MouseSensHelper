Note that due to [UIPI](https://en.wikipedia.org/wiki/User_Interface_Privilege_Isolation) in Windows, if the game is running in administrator mode, then this program needs to also run in administrator mode to simulate mouse input. Using the included resource and manifest files, the compiled binary should automatically request admin privileges when ran.

Libraries required
------------------
1. ucm*
2. interception* (I was unable to link the library statically. The library is linked dynamically i.e. requires a DLL to be bundled with the application. In addition, it has a separate driver installer executable that needs to be bundled.)
3. Qt

*included in the git repo & autoconfigured for use with CMake

Build Instructions For Windows (Visual Studio)
-----------------------------------------------
1. `git clone --recursive https://gitlab.com/tgharib/MouseSensHelper.git`
2. `vcpkg install qt5-base:x64-windows`
3. `cd MouseSensHelper && mkdir build && cd build`
4. `cmake .. -DCMAKE_TOOLCHAIN_FILE=C:\vcpkg\scripts\buildsystems\vcpkg.cmake -G"Visual Studio 16 2019"`
5. Open up the Visual Studio solution file and compile.

TODO
-----
* GNU/Linux port
* Unbundle interception installer
* Clean up code
* Add multiple hotkey methods in case of anti-cheat: GlobalHotkey, GetAsyncKeyState, Userspace Hooks, etc
* Add multiple mouse input methods in case of anti-cheat: SendInput/mouseevent, Userspace hooks, Interception driver, Kernel hooks, etc
* Add a third method of simulating raw input for windows that register to take raw input? (Doesn't seem possible: https://stackoverflow.com/a/22593420)