#include "program.h"
#include <thread>
#ifdef _MSC_VER
#define NOMINMAX  // needed to undefine min/max macros under MSVC compiler
#endif
#include <windows.h>
#include <QCoreApplication>

Program::Program(DataModel& dataModel) : dataModel(dataModel) {}

Program::~Program() {
    if (context) {
        interception_destroy_context(context);
    }
}

void Program::simulateDots(long long dots) {
    dataModel.getMouseSimulationMethod() == 0 ? simulateDotsSendInputMethod(dots) : simulateDotsInterceptionMethod(dots);
}

void Program::simulateDotsSendInputMethod(long long dots) {
    MOUSEINPUT mouseInput = {static_cast<LONG>(dots), 0, 0, MOUSEEVENTF_MOVE, 0, 0};
    std::array<INPUT, 1> singleInputArray = {{INPUT_MOUSE, mouseInput}};
    if (SendInput(static_cast<UINT>(singleInputArray.size()), singleInputArray.data(), sizeof(singleInputArray.at(1))) == 0) {
        QMessageBox::critical(nullptr, "Error",
                              (std::string("SendInput Error: ") + std::to_string(GetLastError())).c_str());
    }
}

void Program::simulateDotsInterceptionMethod(long long dots) {
    initializeInterceptionDriver();

    stroke = {0, INTERCEPTION_MOUSE_MOVE_RELATIVE, 0, static_cast<int>(dots), 0, 0};
    if (interception_send(context, device, (InterceptionStroke*)&stroke, 1) < 1) {
        QMessageBox::critical(nullptr, "Error", "interception_send error");
    }
}

void Program::initializeInterceptionDriver() {
    if (!context) {
        context = interception_create_context();
        QMessageBox::information(nullptr, "Mouse impersonation",
                                 "Please press OK and move the mouse that will be impersonated.");
        interception_set_filter(context, interception_is_mouse, INTERCEPTION_FILTER_MOUSE_MOVE);
        while (true) {
            device = interception_wait(context);
            if (interception_is_mouse(device)) {
                break;
            }
        }
        interception_set_filter(context, interception_is_mouse, INTERCEPTION_FILTER_MOUSE_NONE);
    }
}

void Program::simulateDotsSlowly(long long dots) {
    long long smallIncrement{dots / 700};
    long long remainder{dots};

    if (smallIncrement <= 0) {
        smallIncrement = 1;
    }

    while (remainder > 0) {
        long long subtraction = std::min(smallIncrement, remainder);
        simulateDots(subtraction);
        remainder = remainder - subtraction;
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}

void Program::registerHotkeys() {
    if (!RegisterHotKey(nullptr, 100, 0, VK_DECIMAL)) {
        QMessageBox::critical(nullptr, "Error", "Failed to register . hotkey");
    }
    if (!RegisterHotKey(nullptr, 0, 0, VK_NUMPAD0)) {
        QMessageBox::critical(nullptr, "Error", "Failed to register 0 hotkey");
    }
    if (!RegisterHotKey(nullptr, 2, 0, VK_NUMPAD2)) {
        QMessageBox::critical(nullptr, "Error", "Failed to register 2 hotkey");
    }
    if (!RegisterHotKey(nullptr, 1, 0, VK_NUMPAD1)) {
        QMessageBox::critical(nullptr, "Error", "Failed to register 1 hotkey");
    }
    if (!RegisterHotKey(nullptr, 5, 0, VK_NUMPAD5)) {
        QMessageBox::critical(nullptr, "Error", "Failed to register 5 hotkey");
    }
    if (!RegisterHotKey(nullptr, 4, 0, VK_NUMPAD4)) {
        QMessageBox::critical(nullptr, "Error", "Failed to register 4 hotkey");
    }
    if (!RegisterHotKey(nullptr, 8, 0, VK_NUMPAD8)) {
        QMessageBox::critical(nullptr, "Error", "Failed to register 8 hotkey");
    }
    if (!RegisterHotKey(nullptr, 7, 0, VK_NUMPAD7)) {
        QMessageBox::critical(nullptr, "Error", "Failed to register 7 hotkey");
    }
    if (!RegisterHotKey(nullptr, 99, 0, VK_END)) {
        QMessageBox::critical(nullptr, "Error", "Failed to register END hotkey");
    }
}

void Program::unregisterHotkeys() {
    UnregisterHotKey(nullptr, 100);
    UnregisterHotKey(nullptr, 0);
    UnregisterHotKey(nullptr, 2);
    UnregisterHotKey(nullptr, 1);
    UnregisterHotKey(nullptr, 5);
    UnregisterHotKey(nullptr, 4);
    UnregisterHotKey(nullptr, 8);
    UnregisterHotKey(nullptr, 7);
    UnregisterHotKey(nullptr, 99);
}

long long Program::requestCurrentDotsPer360() {
    QMessageBox messageBox{QMessageBox::Information, "Simulating mouse input",
                           "Do a full 360 rotation towards the right in game using below hotkeys and press END when "
                           "you finish.\n\nNumpad decimal: Simulate 1000 dots to the right\nNumpad 0: Simulate 1000 "
                           "dots to the left\nNumpad 2: Simulate 100 dots to the right\nNumpad 1: Simulate 100 dots to "
                           "the left\nNumpad 5: Simulate 10 dots to the right\nNumpad 4: Simulate 10 dots to the "
                           "left\nNumpad 8: Simulate 1 dot to the right\nNumpad 7: Simulate 1 dot to the left",
                           QMessageBox::Ok};
    messageBox.show();                  // non-blocking message box
    QCoreApplication::processEvents();  // render the message box

    long long currentDotsPer360{0};

    registerHotkeys();

    MSG msg = {0};
    bool breakWhileLoop{false};
    while (!breakWhileLoop && GetMessage(&msg, nullptr, 0, 0) != 0) {
        if (msg.message == WM_HOTKEY) {
            switch (msg.wParam) {
                case 100: {
                    simulateDots(1000);
                    currentDotsPer360 += 1000;
                } break;
                case 0: {
                    simulateDots(-1000);
                    currentDotsPer360 -= 1000;
                } break;
                case 2: {
                    simulateDots(100);
                    currentDotsPer360 += 100;
                } break;
                case 1: {
                    simulateDots(-100);
                    currentDotsPer360 -= 100;
                } break;
                case 5: {
                    simulateDots(10);
                    currentDotsPer360 += 10;
                } break;
                case 4: {
                    simulateDots(-10);
                    currentDotsPer360 -= 10;
                } break;
                case 8: {
                    simulateDots(1);
                    currentDotsPer360 += 1;
                } break;
                case 7: {
                    simulateDots(-1);
                    currentDotsPer360 -= 1;
                } break;
                case 99: {
                    breakWhileLoop = true;
                } break;
            }
        }
    }

    unregisterHotkeys();

    return currentDotsPer360;
}

void Program::convert() {
    if (dataModel.getMouseSimulationMethod() == 1) {
        initializeInterceptionDriver();
    }

    switch (dataModel.getAction()) {
        case 0: {  // Trial & error method
            QMessageBox messageBox{
                QMessageBox::Information, "Simulating mouse input",
                (std::string("Simulating ") + std::to_string(dataModel.getDesiredDotsPer360()) +
                 " dots in 5 seconds... Alt-tab into the game and see if the simulation performs a 360 rotation. If it "
                 "overshoots, decrease your game sensitivity. If it undershoots, increase your game sensitivity.")
                    .c_str(),
                QMessageBox::Ok};
            messageBox.show();                  // non-blocking message box
            QCoreApplication::processEvents();  // render the message box

            std::this_thread::sleep_for(std::chrono::seconds(5));
            simulateDotsSlowly(dataModel.getDesiredDotsPer360());
        } break;
        case 1: {  // Linear method
            double desiredSens{static_cast<double>(requestCurrentDotsPer360()) / dataModel.getDesiredDotsPer360() *
                               dataModel.getGameSens()};
            QMessageBox::information(
                nullptr, "Game sensitivity determined",
                (std::string("Set your game sensitivity to: ") + std::to_string(desiredSens)).c_str());
        } break;
        case 2: {  // Get cm/360
            double currentCMPer360{static_cast<double>(requestCurrentDotsPer360()) / dataModel.getEffectiveMouseDpi() /
                                   dataModel.inchesPerCM};
            QMessageBox::information(nullptr, "Game sensitivity determined",
                                     (std::string("Your cm/360 is: ") + std::to_string(currentCMPer360)).c_str());
        } break;
    }
}
