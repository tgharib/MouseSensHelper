#include "mainwindow.h"
#include <windows.h>
#include <psapi.h>
#include <QMessageBox>
#include <string>

#ifndef UNICODE
using String = std::string;
#else
using String = std::wstring;
#endif

MainWindow::MainWindow(Program& program, DataModel& dataModel) : QMainWindow(), program(program), dataModel(dataModel) {
    ui.setupUi(this);
    ui.statusbar->showMessage("Unsure about a setting? Hover the mouse cursor over it to receive help text.");
    ui.pushButton_uninstallDriver->setVisible(false);
    setWindowFlags(Qt::Window | Qt::WindowMinimizeButtonHint | Qt::WindowCloseButtonHint);
    show();
    setFixedSize(width(), height());

    QMessageBox::information(this, "Welcome to the mouse sensitivity helper!",
                             "This program assists you to convert your physical sensitivity (cm/360) to any "
                             "first-person shooter game sensitivity (e.g. Quake, Unreal Tournament, etc).\n\nNote that "
                             "windows pointer settings, game field of view, and game resolution can potentially affect "
                             "mouse sensitivity. All mouse acceleration is required to be disabled before using this "
                             "tool. It is recommended windows pointer speed is set to 6/11.");
}

bool MainWindow::isInterceptionDriverInstalled() {
    std::array<LPVOID, 1024> drivers{{0}};
    DWORD bytesNeeded{0};
    if (EnumDeviceDrivers(drivers.data(), sizeof(drivers), &bytesNeeded) && bytesNeeded < sizeof(drivers)) {
        std::array<TCHAR, 1024> driverName{{0}};
        int cDrivers = bytesNeeded / sizeof(drivers.at(0));
        for (int i = 0; i < cDrivers; i++) {
            if (GetDeviceDriverBaseName(drivers[i], driverName.data(), driverName.size())) {
                String driverNameString(driverName.data());
                std::transform(driverNameString.begin(), driverNameString.end(), driverNameString.begin(), ::tolower);
                if (driverNameString.find("mouse.sys") != std::string::npos) {
                    return true;
                }
            }
        }
        return false;
    } else {
        QMessageBox::critical(this, "Error",
                              (std::string("EnumDeviceDrivers failed; array size needed is ") +
                               std::to_string(bytesNeeded / sizeof(LPVOID)) + std::to_string(GetLastError()))
                                  .c_str());
        return false;
    }
}

void MainWindow::on_comboBox_mouseSimulationMethod_currentIndexChanged(int index) {
    if (index == 1) {
        if (isInterceptionDriverInstalled()) {
            QMessageBox::information(this, "Interception driver method selected",
                                     "Interception driver detected (which is required for this mode). Close any other "
                                     "software that uses the interception driver as they might interfere.");
            ui.pushButton_uninstallDriver->setVisible(true);
        } else {
            if (QMessageBox::Yes == QMessageBox(QMessageBox::Information, "Interception driver method selected",
                                                "You have selected the interception driver mouse simulation method but "
                                                "the interception driver was not detected (which is required for this "
                                                "mode). Would you like to install the interception driver?",
                                                QMessageBox::Yes | QMessageBox::No)
                                        .exec()) {
                ShellExecute(nullptr, "open", "install-interception.exe", "/install", nullptr, SW_SHOWDEFAULT);
                QMessageBox::critical(this, "Interception driver method selected",
                                      "You must reboot for driver installation to take effect.");
            }
        }
    } else {
        ui.pushButton_uninstallDriver->setVisible(false);
    }
}

void MainWindow::on_comboBox_action_currentIndexChanged(int index) {
    switch (index) {
        case 0: {  // Trial & error method
            ui.lineEdit_DPI->setEnabled(dataModel.getEnabledParametersArray().at(0).at(0));
            ui.label_DPI->setEnabled(dataModel.getEnabledParametersArray().at(0).at(0));
            ui.lineEdit_cmPer360->setEnabled(dataModel.getEnabledParametersArray().at(0).at(1));
            ui.label_cmPer360->setEnabled(dataModel.getEnabledParametersArray().at(0).at(1));
            ui.lineEdit_gameSens->setEnabled(dataModel.getEnabledParametersArray().at(0).at(2));
            ui.label_gameSens->setEnabled(dataModel.getEnabledParametersArray().at(0).at(2));
        } break;
        case 1: {  // Linear method
            ui.lineEdit_DPI->setEnabled(dataModel.getEnabledParametersArray().at(1).at(0));
            ui.label_DPI->setEnabled(dataModel.getEnabledParametersArray().at(1).at(0));
            ui.lineEdit_cmPer360->setEnabled(dataModel.getEnabledParametersArray().at(1).at(1));
            ui.label_cmPer360->setEnabled(dataModel.getEnabledParametersArray().at(1).at(1));
            ui.lineEdit_gameSens->setEnabled(dataModel.getEnabledParametersArray().at(1).at(2));
            ui.label_gameSens->setEnabled(dataModel.getEnabledParametersArray().at(1).at(2));
        } break;
        case 2: {  // Get cm/360
            ui.lineEdit_DPI->setEnabled(dataModel.getEnabledParametersArray().at(2).at(0));
            ui.label_DPI->setEnabled(dataModel.getEnabledParametersArray().at(2).at(0));
            ui.lineEdit_cmPer360->setEnabled(dataModel.getEnabledParametersArray().at(2).at(1));
            ui.label_cmPer360->setEnabled(dataModel.getEnabledParametersArray().at(2).at(1));
            ui.lineEdit_gameSens->setEnabled(dataModel.getEnabledParametersArray().at(2).at(2));
            ui.label_gameSens->setEnabled(dataModel.getEnabledParametersArray().at(2).at(2));
        } break;
    }
}

void MainWindow::on_pushButton_convert_clicked() {
    if (dataModel.setState(*this)) {
        program.convert();
    }
}

void MainWindow::on_pushButton_uninstallDriver_clicked() {
    if (QMessageBox::Yes == QMessageBox(QMessageBox::Information, "Uninstall the driver?",
                                        "Do you want to uninstall the interception driver?",
                                        QMessageBox::Yes | QMessageBox::No)
                                .exec()) {
        ShellExecute(nullptr, "open", "install-interception.exe", "/uninstall", nullptr, SW_SHOWDEFAULT);
        QMessageBox::critical(this, "Driver uninstallation",
                              "You must reboot for driver uninstallation to take effect.");
    }
}
