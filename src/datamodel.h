#pragma once
#include <array>
#include "mainwindow.h"

class MainWindow;

class DataModel {
   public:
    static constexpr double inchesPerCM{0.3937007874};

    bool setState(MainWindow& mainWindow);
    const std::array<std::array<bool, 3>, 3>& getEnabledParametersArray() const;
    int getAction() const;
    double getCmPer360() const;
    double getEffectiveMouseDpi() const;
    double getGameSens() const;
    int getMouseSimulationMethod() const;
    long long getDesiredDotsPer360() const;

   private:
    std::array<std::array<bool, 3>, 3> enabledParametersArray{
        {{true, true, false}, {true, true, true}, {true, false, false}}};
    // inner index is dpi, cmPer360, gameSens and outer index is action number

    int mouseSimulationMethod{0};
    int action{0};
    double effectiveMouseDPI{0};
    double cmPer360{0};
    double gameSens{0};

    long long desiredDotsPer360{0};
};
