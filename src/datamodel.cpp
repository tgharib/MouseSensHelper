#include "datamodel.h"
#include <math.h>
#include <QMessageBox>

bool DataModel::setState(MainWindow& mainWindow) {
    bool ok{false};

    mouseSimulationMethod = mainWindow.ui.comboBox_mouseSimulationMethod->currentIndex();

    action = mainWindow.ui.comboBox_action->currentIndex();

    effectiveMouseDPI = mainWindow.ui.lineEdit_DPI->text().toDouble(&ok);
    if (enabledParametersArray.at(action).at(0) && (!ok || mainWindow.ui.lineEdit_DPI->text().isEmpty())) {
        QMessageBox::critical(&mainWindow, "Parsing error", "Failed to parse effective mouse DPI.");
        return false;
    }

    cmPer360 = mainWindow.ui.lineEdit_cmPer360->text().toDouble(&ok);
    if (enabledParametersArray.at(action).at(1) && (!ok || mainWindow.ui.lineEdit_cmPer360->text().isEmpty())) {
        QMessageBox::critical(&mainWindow, "Parsing error", "Failed to parse your cm/360.");
        return false;
    }

    gameSens = mainWindow.ui.lineEdit_gameSens->text().toDouble(&ok);
    if (enabledParametersArray.at(action).at(2) && (!ok || mainWindow.ui.lineEdit_gameSens->text().isEmpty())) {
        QMessageBox::critical(&mainWindow, "Parsing error", "Failed to parse game sensitivity.");
        return false;
    }

    if (action == 0 || action == 1) {
        desiredDotsPer360 = static_cast<long long>(round(effectiveMouseDPI * cmPer360 * inchesPerCM));
        if (desiredDotsPer360 <= 0) {
            QMessageBox::critical(&mainWindow, "Too low DPI or cm/360",
                                  "At the DPI given, your desired cm/360 is too low.");
            return false;
        }
    }

    return true;
}

const std::array<std::array<bool, 3>, 3>& DataModel::getEnabledParametersArray() const {
    return enabledParametersArray;
}

int DataModel::getAction() const { return action; }

double DataModel::getCmPer360() const { return cmPer360; }

double DataModel::getEffectiveMouseDpi() const { return effectiveMouseDPI; }

double DataModel::getGameSens() const { return gameSens; }

int DataModel::getMouseSimulationMethod() const { return mouseSimulationMethod; }

long long DataModel::getDesiredDotsPer360() const { return desiredDotsPer360; }
