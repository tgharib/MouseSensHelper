#pragma once
#include <QMainWindow>
#include "datamodel.h"
#include "program.h"
#include "ui_mainwindow.h"

class Program;
class DataModel;

class MainWindow : public QMainWindow {
    Q_OBJECT

   public:
    MainWindow(Program& program, DataModel& dataModel);
    Ui::MainWindow ui;

   private slots:
    void on_comboBox_mouseSimulationMethod_currentIndexChanged(int index);
    void on_comboBox_action_currentIndexChanged(int index);
    void on_pushButton_convert_clicked();
    void on_pushButton_uninstallDriver_clicked();

   private:
    Program& program;
    DataModel& dataModel;
    bool isInterceptionDriverInstalled();
};
