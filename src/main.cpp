#include <windows.h>
#include <QApplication>
#include <QtPlugin>
#include "mainwindow.h"

namespace {
#ifndef LLMHF_LOWER_IL_INJECTED
#define LLMHF_LOWER_IL_INJECTED 0x00000002
#endif

HHOOK hook{nullptr};

LRESULT CALLBACK LowLevelMouseProc(int nCode, WPARAM wParam, LPARAM lParam) {
    if (nCode == HC_ACTION && wParam == WM_MOUSEMOVE) {
        auto pHookStruct = reinterpret_cast<MSLLHOOKSTRUCT*>(lParam);

        if (pHookStruct->flags & LLMHF_INJECTED) {
            pHookStruct->flags &= ~LLMHF_INJECTED;
            // MessageBox(NULL, "detected", "detected", MB_ICONERROR);
        }

        if (pHookStruct->flags & LLMHF_LOWER_IL_INJECTED) {
            pHookStruct->flags &= ~LLMHF_LOWER_IL_INJECTED;
        }
    }
    return CallNextHookEx(0, nCode, wParam, lParam);
}

void installHook() {
    hook = SetWindowsHookEx(WH_MOUSE_LL, &LowLevelMouseProc, nullptr, 0);
    if (!hook) {
        MessageBox(nullptr, "failed to install hook", "fail", MB_ICONERROR);
    }
}

void hookThread() {
    installHook();
    MSG gmsg{};
    while (GetMessage(&gmsg, nullptr, 0, 0)) {
    }
    UnhookWindowsHookEx(hook);
}
}  // namespace

int main(int argc, char** argv) {
    CreateThread(nullptr, 0, (LPTHREAD_START_ROUTINE)hookThread, nullptr, 0, nullptr);

    QApplication app(argc, argv);

    DataModel dataModel{};
    Program program{dataModel};

    MainWindow mainWindow{program, dataModel};
    return app.exec();  // GUI event handling
}
