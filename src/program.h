#pragma once
#include <interception.h>
#include <QMessageBox>
#include "datamodel.h"

class DataModel;

class Program {
   public:
    Program(DataModel& dataModel);
    ~Program();
    void convert();

   private:
    DataModel& dataModel;
    InterceptionContext context{nullptr};
    InterceptionDevice device{};
    InterceptionMouseStroke stroke{};

    void simulateDotsSlowly(long long dots);
    void simulateDots(long long dots);
    void simulateDotsSendInputMethod(long long dots);
    void simulateDotsInterceptionMethod(long long dots);
    void registerHotkeys();
    void unregisterHotkeys();
    long long requestCurrentDotsPer360();
    void initializeInterceptionDriver();
};
