This program assists you to convert your physical sensitivity (inches/360° or cm/360°) to a first-person shooter game sensitivity (e.g. Quake, Unreal Tournament, etc). This program currently only supports Windows. GNU/Linux support may be added in the future. This program should be ran after you're already in-game so that the program is the last program to hook into Windows's low-level hooks.

* Refer to [docs/demo.mp4](https://gitlab.com/tgharib/MouseSensHelper/blob/master/docs/demo.mp4) for a demo of the program in action.
* Refer to [docs/inner-workings.pdf](https://gitlab.com/tgharib/MouseSensHelper/blob/master/docs/inner-workings.pdf) to understand how the program works mathematically.
* Refer to [docs/developers.md](https://gitlab.com/tgharib/MouseSensHelper/blob/master/docs/developers.md) if you're a software developer.

This program has two methods to simulate mouse input:

1. Windows SendInput

[This blog post](https://blogs.msdn.microsoft.com/oldnewthing/20140213-00/?p=1773) provides a nice diagram of the Windows userpace input stack. As can be seen from the diagram, SendInput simulates input at the deepest level of userspace but not in kernelspace. The input passes through Windows's low-level userspace hooks and if an application hooks there, they can detect injected input via the LLMHF_INJECTED flag. This is why this program also hooks there and clears the LLMHF_INJECTED flag. However, from the diagram, before the low-level hooks are executed, there is a raw input thread that runs which can also [detect injected input](https://stackoverflow.com/a/37422944). In addition, if the program periodically rehooks into the Windows hooks, then our method to clear the LLMHF_INJECTED flag is nullified. Therefore, this method is a lost cause and can only work in games without strong anti-cheat or in games where the anti-cheat is purposefully disabled.

2. [Interception driver](https://github.com/oblitum/Interception/)

The interception driver is a signed driver that sits in the kernel and is a [filter driver](https://en.wikipedia.org/wiki/Filter_driver). This is the absolute lowest-level you can go without implementing a mouse driver. Because the driver is signed, the OS doesn't need to be in test mode but it does require installing a driver and rebooting before being able to use this method.